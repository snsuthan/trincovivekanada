<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'trincovivekanada');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`zz),s.W7yPSpA|;K[?L4),#XGCK`7dqhm?w!^9:q{5%z.HrcMS+O=rIIj1^*YLi');
define('SECURE_AUTH_KEY',  ';_~<j{EA8iC5vkC=![AZ_s[ C-fyVg|II90~OiQy:tegTf]xsNDNmQ8qS2R|>Z+%');
define('LOGGED_IN_KEY',    '3BG_I8|] A+ezZ2vu@`]0(Q6*&G.mPge8QH.2i5+;DFcwptI ~+na9~?O@WX4S{`');
define('NONCE_KEY',        'kmvZzV<)&{A*2A:`;UQ?c!`=pC+~E/BdmHDWniMz4klVb]DqHG{.N$|!]bs):qgZ');
define('AUTH_SALT',        'uxX37]#p>^v{sFYciv_v_@twhU1J,<i#]eC^{!*&Hqfwv]x5/ ;@IOszld!_dE+?');
define('SECURE_AUTH_SALT', 'xWqpB69LB`dWB?uq}J&.h^]FbjF~L {u,;5l=PM>xSD &%vf.JZav7[[$?~2-T6o');
define('LOGGED_IN_SALT',   '=O!p*XP+[2PS[Qfn;6*gLvXGGD:.AxX,$^JE~4^NhO3hA<)|W)(Sd,P}#/tA:{`T');
define('NONCE_SALT',       'n[6^I^+I8G)9Sq=*i404x$fnF-NBzH2L,4U3p:[IUKDC38)]dA$_PyF?pmK}u1%%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
