<?php
function parse_calendar_func($atts, $content){
		$cat 		=isset($atts['cat']) ? $atts['cat'] : '';
		$tag 	= isset($atts['tag']) ? $atts['tag'] : '';
		$exclude 	= isset($atts['exclude']) ? $atts['exclude'] : '';
		$ignore 	= isset($atts['ignore']) ? $atts['ignore'] : '';
		$month 		=isset($atts['month']) ? $atts['month'] : '';
		$years 	= isset($atts['year']) ? $atts['year'] : '';
		if($month!=''){
			if($years!=''){
				$date = date( $years.'-'.$month.'-d');
			}else{
				$date = date( 'Y-'.$month.'-d');
			}
		}else{
			$date = date('Y-m-d');
		}
		

	wp_reset_postdata();
	//$data_rs = str_replace('\/', '/', json_encode($data_rs));
	wp_enqueue_style('style-calendar',plugins_url('/u-event/css/style-calendar.css'));
	wp_enqueue_style('owl-carousel',plugins_url('/u-event/js/owl-carousel/owl.carousel.css'));
	wp_enqueue_style('owl-carousel-theme',plugins_url('/u-event/js/owl-carousel/owl.theme.css'));
	wp_enqueue_style('owl-carousel-transitions',plugins_url('/u-event/js/owl-carousel/owl.transitions.css'));
	wp_enqueue_script( 'calendar',plugins_url('/u-event/js/calendar.js') , array(), '', true );?>

          <div id="calendar-box"> 
          <style type="text/css">
		  #main-nav .main-menu.affix{ z-index:9999}
		  </style>          	  
              <div class="header-content">
                  <button class="btn btn-primary" data-calendar-nav="prev"><i class="fa fa-chevron-left"></i></button>
                  <h3></h3>          
                  <button class="btn btn-primary" data-calendar-nav="next"><i class="fa fa-chevron-right"></i></button>
              </div>
              <div id="stm-calendar-id"></div>
              <input type="hidden" id="check-id-carousel" value="-1"> <!--Giá trị ban đầu bằng -1 đích đến khi chọn event -> sẽ tới slide carousel-->
              <input type="hidden" id="check-event-slidedown" value="0"> <!--Giá trị ban đầu bằng 0 / bằng 0 cho mở carousel / 1 tắt-->
              <input type="hidden" id="check-event-slidedown-1" value="0"> <!--Giá trị ban đầu bằng 0 / ngày tháng khi click mở carousel-->
              <input type="hidden" id="check-monthdata" value="<?php echo $date;?>">
			  <input type="hidden" id="month-url" value="<?php echo plugins_url('/monthview/', __FILE__); ?>"> 	
              <input type="hidden" id="check-jsondata" value="<?php echo home_url( 'wp-admin/admin-ajax.php' )?>">
              <input type="hidden" id="action_data" value="1">
              <input type="hidden" id="action_cat" value="<?php echo $cat;?>">
              <input type="hidden" id="action_tag" value="<?php echo $tag;?>">
              <input type="hidden" id="action_exclude" value="<?php echo $exclude;?>">
              <input type="hidden" id="action_ignore" value="<?php echo $ignore;?>">
              <div id="calendar-loading">
              	<div class="windows8">
                    <div class="wBall" id="wBall_1">
                        <div class="wInnerBall"></div>
                    </div>
                    <div class="wBall" id="wBall_2">
                        <div class="wInnerBall"></div>
                    </div>
                    <div class="wBall" id="wBall_3">
                        <div class="wInnerBall"></div>
                    </div>
                    <div class="wBall" id="wBall_4">
                        <div class="wInnerBall"></div>
                    </div>
                    <div class="wBall" id="wBall_5">
                        <div class="wInnerBall"></div>
                    </div>
                </div>
              </div>
          </div> 
      	<!--Khung chứa bảng ngày tháng-->
        
        <!--Thanh điều hướng, có thể bỏ đi mà ko gây lỗi cho các khung trên-->        
          <div id="calendar-options">
              <div class="left-options">
                <a href="javascript:;" class="active" id="monthview-calendar">Calendar View</a> &nbsp; | &nbsp; <a href="javascript:;" id="listview-calendar">List View</a>
              </div>
              <div class="right-options">
                <ul>
                    <li><a href="javascript:;">All Speakers <i class="fa fa-angle-down"></i></a> &nbsp; &nbsp;| 
                        <ul>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                        </ul>
                    </li>
                    <li> &nbsp; &nbsp; <a href="javascript:;">All Location <i class="fa fa-angle-down"></i></a>
                        <ul>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                            <li><a href="javascript:;">Test test test test</a></li>
                        </ul>
                    </li>
                </ul>
              </div>
          </div>
      	<!--Thanh điều hướng-->
    <?php

}
add_shortcode( 'u-calendar', 'parse_calendar_func' );
add_action( 'after_setup_theme', 'reg_u_calendar' );
function reg_u_calendar(){
	if(function_exists('wpb_map')){
		$current_year = date("Y");
		$years = array();
		for($i=-11; $i<=10; $i++){
			if($i==-11){
				$years[''] = '';
			}else{
				$years[$current_year+$i] = ($current_year+$i);
			}
		}
		$month = array();
		for($i=0; $i<=12; $i++){
			if($i==0){
				$month[''] = '';
			}else{
				if($i<10){
					$month['0'.$i] = ('0'.$i);
				}else{
					$month[$i] = ($i);
				}
			}
		}
		wpb_map( array(
		   "name" => __("Calendar",'cactusthemes'),
		   "base" => "u-calendar",
		   "class" => "",
		   "controls" => "full",
		   "category" => 'Content',
		   "icon" => "icon-calendar",
		   "params" => array(
			   array(
				  "type" => "dropdown",
				  "holder" => "div",
				  "heading" => __("Month", "cactusthemes"),
				  "param_name" => "month",
				  "value" => $month,
				  "description" => ""
			  ),
			  array(
				  "type" => "dropdown",
				  "holder" => "div",
				  "heading" => __("Year", "cactusthemes"),
				  "param_name" => "year",
				  "value" => $years,
				  "description" => ""
			  ),
			  array(
				"type" => "textfield",
				"heading" => __("Categories", "cactusthemes"),
				"param_name" => "cat",
				"value" => "",
				"description" => __("", "cactusthemes"),
			  ),
			  array(
				"type" => "textfield",
				"heading" => __("Tags", "cactusthemes"),
				"param_name" => "tag",
				"value" => "",
				"description" => __("", "cactusthemes"),
			  ),
			  array(
				"type" => "textfield",
				"heading" => __("Exclude", "cactusthemes"),
				"param_name" => "exclude",
				"value" => "",
				"description" => __("", "cactusthemes"),
			  ),
			  array(
				 "type" => "dropdown",
				 "holder" => "div",
				 "class" => "",
				 "heading" => __("Ignore", 'cactusthemes'),
				 "param_name" => "ignore",
				 "value" => array(
				 	__('', 'cactusthemes') => '',
					__('Upcoming', 'cactusthemes') => 'upcoming',
					__('Recent', 'cactusthemes') => 'recent',
				 ),
				 "description" => __('','cactusthemes')
			  ),
		   )
		) );
	}
}