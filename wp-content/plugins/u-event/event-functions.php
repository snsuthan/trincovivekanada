<?php
//add_action( 'save_post', 'ev_save_postdata' );
function ev_save_postdata($post_id ){
	if('u_event' != get_post_type())
	return;
	$startdate_date = $_POST['startdate_date'];
	$enddate_date = $_POST['enddate_date'];
	$recurrence = $_POST['recurrence'];
	$startdate_date = strtotime($startdate_date['cmb-field-0']);//exit;
	$enddate_date = strtotime($enddate_date['cmb-field-0']);//exit;
	if(!update_post_meta( $post_id, 'startdate_date_ux', $startdate_date)){
		add_post_meta( $post_id, 'startdate_date_ux', $startdate_date, true);
	}else{
		update_post_meta( $post_id, 'startdate_date_ux', $startdate_date);
	}
	if(!update_post_meta( $post_id, 'enddate_date_ux', $enddate_date) ){
		add_post_meta( $post_id, 'enddate_date_ux', $enddate_date, true);
	}else{
		update_post_meta( $post_id, 'enddate_date_ux', $enddate_date);
	}	
}

function calendar_data_json() {
	if(isset($_GET['cal_json'])&& $_GET['cal_json']==1){
		$tag = $_GET['tag'];
		$args = array(
			'post_type' => 'u_event',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'post__not_in' => array($_GET['exclude']),
			'ignore_sticky_posts' => 1,
		);
		$cat = $_GET['cat'];
		$ignore = $_GET['ignore'];
		if(isset($_GET['month-data'])&& $_GET['listview']==1){
			 $month_start = date($_GET['month-data']) ;
			 $month_end = date("Y-m-t", strtotime($month_start));;
			if($order==''){$order='DESC';}
			if($ignore=='upcoming'){ $month_start=date('m/d/Y');}
			elseif($ignore=='recent'){ $month_end=date('m/d/Y');}
			$args += array(
					 'meta_key' => 'u-startdate',
					 'orderby' => 'meta_value_num',
					 'order' => $order,
					 'meta_query' => array(
					 'relation' => 'AND',
					  array('key'  => 'u-enddate',
						   'value' => strtotime($month_start),
						   'compare' => '>='),
					  array('key'  => 'u-startdate',
						   'value' => strtotime($month_end),
						   'compare' => '<=')
					 )
			);
		}else{
		$time_now =  strtotime("now");
			if($ignore=='recent'){
				if($order==''){$order='ASC';}
				$args += array('meta_key' => 'u-startdate', 'meta_value' => $time_now, 'meta_compare' => '>','orderby' => 'meta_value_num', 'order' => $order);
			}elseif($ignore=='upcoming'){
				if($order==''){$order='DESC';}
				$args += array('meta_key' => 'u-startdate', 'meta_value' => $time_now, 'meta_compare' => '<','orderby' => 'meta_value_num', 'order' => $order);
			}
		}
		if(!is_array($cat) && $cat!='') {
			$cats = explode(",",$cat);
			if(is_numeric($cats[0])){
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'u_event_cat',
						'field'    => 'id',
						'terms'    => $cats,
						'operator' => 'IN',
					)
				);
			}else{
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'u_event_cat',
						'field'    => 'slug',
						'terms'    => $cats,
						'operator' => 'IN',
					)
				);
			}
		}elseif(count($cat) > 0 && $cat!=''){
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'u_event_cat',
					'field'    => 'id',
					'terms'    => $cat,
					'operator' => 'IN',
				)
			);
		}
		//tag
		if($tag) {
			$tags = explode(",",$tag);
			$args['tax_query'][] = array(
				'taxonomy' => 'u_event_tags',
				'field'    => 'slug',
				'terms'    => $tags,
				'operator' => 'IN',
			);
		}

		$the_query = new WP_Query( $args );
		$it = $the_query->post_count;
		$success =0;
		$data_rs = array();
		$rs=array();
		$success = 1;
		if($the_query->have_posts()){
			$date_format = get_option('date_format');
			$hour_format = get_option('time_format');
			while($the_query->have_posts()){ $the_query->the_post();
				$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'thumb_255x255' );
				$startdate = get_post_meta(get_the_ID(),'u-startdate', true );
				if($startdate){
					$startdate_cal = gmdate("Ymd\THis", $startdate);
					$startdate = gmdate("Y-m-d\TH:i:s\Z", $startdate);// convert date ux
					$con_date = new DateTime($startdate);
					$con_hour = new DateTime($startdate);
					$start_datetime = $con_date->format($date_format);
					$start_hourtime = $con_date->format($hour_format);
				}
				$enddate = get_post_meta(get_the_ID(),'u-enddate', true );
				//$endtime = get_post_meta(get_the_ID(),'endtime_time', true );
				if($enddate){
					$enddate_cal = gmdate("Ymd\THis", $enddate);
					$enddate = gmdate("Y-m-d\TH:i:s\Z", $enddate);
					$conv_enddate = new DateTime($enddate);
					$conv_hourtime = new DateTime($enddate);
					$end_datetime = $conv_enddate->format($date_format);
					$end_hourtime = $conv_enddate->format($hour_format);
				}
				$color_event = get_post_meta(get_the_ID(),'color_event', true );
				$color_def =array('#ed1c24','#00FF00','#0000FF','#660066','#000000');
				if($color_event==''){$color_event = $color_def[array_rand($color_def)];}
				$ar_rs= array(
					  'id'=> get_the_ID(),
					  'title'=> get_the_title(),
					  'url'=> get_permalink(),
					  'class'=> $color_event,
					  'start'=>get_post_meta(get_the_ID(),'u-startdate', true )*1000,
					  'end'=>get_post_meta(get_the_ID(),'u-enddate', true )*1000,
					  'startDate'=>$start_datetime.' '.$start_hourtime,
					  'endDate'=>$end_datetime.' '.$end_hourtime,
					  'picture' => $image_src[0],
					  'location' => get_post_meta(get_the_ID(),'u-adress', true ),
					  'buyticket' => get_post_meta(get_the_ID(),'cost', true ),
				  );
				  //array_push($rs,$ar_rs);
				  $rs[]=$ar_rs;
			}
			
		}
		$data_rs = array(
			'success'=>$success,
			'result'=> $rs,
		);
		//print_r($rs);
		echo str_replace('\/', '/', json_encode($data_rs));
		exit;
		}
}
add_action( 'wp_ajax_calendar_data', 'calendar_data_json' );
add_action( 'wp_ajax_nopriv_calendar_data', 'calendar_data_json' );
function uni_events_ical() {
	if(isset($_GET['ical_id'])&& $_GET['ical_id']>0){
		// - start collecting output -
		ob_start();
		
		// - file header -
		header('Content-type: text/calendar');
		header('Content-Disposition: attachment; filename="uni ical.ics"');
		global $post;
		// - content header -
		?>
        <?php
		$content = "BEGIN:VCALENDAR\r\n";
		$content .= "VERSION:2.0\r\n";
		$content .= 'PRODID:-//'.get_bloginfo('name')."\r\n";
		$content .= "CALSCALE:GREGORIAN\r\n";
		$content .= "METHOD:PUBLISH\r\n";
		$content .= 'X-WR-CALNAME:'.get_bloginfo('name')."\r\n";
		$content .= 'X-ORIGINAL-URL:'.get_permalink($_GET['ical_id'])."\r\n";
		$content .= 'X-WR-CALDESC:'.get_the_title($_GET['ical_id'])."\r\n";
		?>
		<?php
		
		// - grab date barrier -
		//$today6am = strtotime('today 6:00') + ( get_option( 'gmt_offset' ) * 3600 );
		//$limit = get_option('pubforce_rss_limit');
		
		// - query -
		//global $wpdb;
		// - loop -
		//setup_postdata($post);
		$date_format = get_option('date_format');
		$hour_format = get_option('time_format');
		$startdate = get_post_meta($_GET['ical_id'],'u-startdate', true );
		if($startdate){
			$startdate = gmdate("Ymd\THis", $startdate);// convert date ux
		}
		$enddate = get_post_meta($_GET['ical_id'],'u-enddate', true );
		if($enddate){
			$enddate = gmdate("Ymd\THis", $enddate);
		}
		
		// - custom variables -
		//$custom = get_post_custom(get_the_ID());
		//$sd = $custom["tf_events_startdate"][0];
		//$ed = $custom["tf_events_enddate"][0];
		//
		//// - grab gmt for start -
		//$gmts = date('Y-m-d H:i:s', $con_date);
		$gmts = get_gmt_from_date($startdate); // this function requires Y-m-d H:i:s, hence the back & forth.
		$gmts = strtotime($gmts);
		
		// - grab gmt for end -
		//$gmte = date('Y-m-d H:i:s', $conv_enddate);
		$gmte = get_gmt_from_date($enddate); // this function requires Y-m-d H:i:s, hence the back & forth.
		$gmte = strtotime($gmte);
		
		// - Set to UTC ICAL FORMAT -
		$stime = date('Ymd\THis', $gmts);
		$etime = date('Ymd\THis', $gmte);
		
		// - item output -
		?>
        <?php
		$content .= "BEGIN:VEVENT\r\n";
		$content .= 'DTSTART:'.$startdate."\r\n";
		$content .= 'DTEND:'.$enddate."\r\n";
		$content .= 'SUMMARY:'.get_the_title($_GET['ical_id'])."\r\n";
		$content .= 'DESCRIPTION:'.get_post($_GET['ical_id'])->post_excerpt."\r\n";
        $content .= 'LOCATION:'.get_post_meta($_GET['ical_id'],'u-adress', true )."\r\n";
		$content .= "END:VEVENT\r\n";
		$content .= "END:VCALENDAR\r\n";
		// - full output -
		$tfeventsical = ob_get_contents();
		ob_end_clean();
		echo $content;
		exit;
		}
}
add_action('init','uni_events_ical');
/**
 * Add a duplicate post link.
 *
 */
add_filter( 'post_row_actions', 'event_duplicator_action_row', 10, 2 );
function event_duplicator_action_row( $actions, $post ){

	// Get the post type object
	$post_type = get_post_type_object( $post->post_type );
	if ( $post->post_type != 'u_event' )
			return $actions;
	// Create a nonce & add an action
  	$actions['duplicate_post'] = '<a href="'.wp_nonce_url( admin_url( 'edit.php?post_type=u_event&duplicate_event=' . $post->ID ), 'duplicate_event' . $post->ID, '_wpnonce' ).'">Duplicate '.$post_type->labels->singular_name.'</a>';

	return $actions;
}
function duplicate_event(){
	if (!isset($_GET['trashed'])&&isset($_GET['_wpnonce']) && wp_verify_nonce($_GET['_wpnonce'], 'duplicate_event' . $_GET['duplicate_event'])) {
		if($_GET['duplicate_event'] && current_user_can('administrator') && is_admin){
			$duplicate = get_post( $_GET['duplicate_event'], 'ARRAY_A' );
			
			$duplicate['post_title'] = $duplicate['post_title'].' Copy';
		
		
			// Remove some of the keys
			unset( $duplicate['ID'] );
			unset( $duplicate['guid'] );
			unset( $duplicate['comment_count'] );
		
			// Insert the post into the database
			$duplicate_id = wp_insert_post( $duplicate );
			$taxonomies = get_object_taxonomies( $duplicate['post_type'] );
			foreach( $taxonomies as $taxonomy ) {
				$terms = wp_get_post_terms( $_GET['duplicate_event'], $taxonomy, array('fields' => 'names') );
				wp_set_object_terms( $duplicate_id, $terms, $taxonomy );
			}
			$custom_fields = get_post_custom( $_GET['duplicate_event']);
			foreach ( $custom_fields as $key => $value ) {
				  add_post_meta( $duplicate_id, $key, maybe_unserialize($value[0]) );
			}
		}
	}
}
add_action('init', 'duplicate_event');
?>