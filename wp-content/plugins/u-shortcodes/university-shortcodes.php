<?php
   /*
   Plugin Name: University - Shortcodes
   Plugin URI: http://www.cactusthemes.com
   Description: University - Shortcodes
   Version: 1.9.6
   Author: Cactusthemes
   Author URI: http://www.cactusthemes.com
   License: Commercial
   */
if ( ! defined( 'U_SHORTCODE_BASE_FILE' ) )
    define( 'U_SHORTCODE_BASE_FILE', __FILE__ );
if ( ! defined( 'U_SHORTCODE_BASE_DIR' ) )
    define( 'U_SHORTCODE_BASE_DIR', dirname( U_SHORTCODE_BASE_FILE ) );
if ( ! defined( 'U_SHORTCODE_PLUGIN_URL' ) )
    define( 'U_SHORTCODE_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

/* ================================================================
 *
 * 
 * Class to register shortcode with TinyMCE editor
 *
 * Add to button to tinyMCE editor
 *
 */
class CactusThemeShortcodes{
	
	function __construct()
	{
		add_action('init',array(&$this, 'init'));
	}
	
	function init(){		
		if(is_admin()){
			// CSS for button styling
			wp_enqueue_style("ct_shortcode_admin_style", U_SHORTCODE_PLUGIN_URL . '/shortcodes/shortcodes.css');
		}

		if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
	    	return;
		}
	 
		if ( get_user_option('rich_editing') == 'true' ) {
			add_filter( 'mce_external_plugins', array(&$this, 'regplugins'));
			add_filter( 'mce_buttons_3', array(&$this, 'regbtns') );
			
			// remove a button. Used to remove a button created by another plugin
			remove_filter('mce_buttons_3', array(&$this, 'remobtns'));
		}
	}
	
	function remobtns($buttons){
		// add a button to remove
		// array_push($buttons, 'ct_shortcode_collapse');
		return $buttons;	
	}
	
	function regbtns($buttons)
	{
		array_push($buttons, 'shortcode_button_cactus');
		array_push($buttons, 'shortcode_button');
		array_push($buttons, 'shortcode_blog');
		array_push($buttons, 'shortcode_post_carousel');
		array_push($buttons, 'shortcode_post_grid');
		array_push($buttons, 'shortcode_testimonial');
		array_push($buttons, 'shortcode_dropcap');
		array_push($buttons, 'shortcode_textbox');
		array_push($buttons, 'shortcode_course_list');
		array_push($buttons, 'shortcode_member');
		//array_push($buttons, 'shortcode_headline');	
		array_push($buttons, 'shortcode_heading');
		array_push($buttons, 'shortcode_countdown');
		//array_push($buttons, 'shortcode_padding');
		array_push($buttons, 'cactus_compare_table');		
		array_push($buttons, 'shortcode_post_scroller');
		array_push($buttons, 'shortcode_video_banner');		
		return $buttons;
	}
	
	function regplugins($plgs)
	{
		$plgs['shortcode_button_cactus'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/button-shortcode.js';
		$plgs['shortcode_button'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/button.js';
		$plgs['shortcode_blog'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/blog.js';
		$plgs['shortcode_post_carousel'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/post-carousel.js';
		$plgs['shortcode_post_grid'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/post-grid.js';
		$plgs['shortcode_textbox'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/textbox.js';
		$plgs['shortcode_testimonial'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/testimonial.js';
		$plgs['shortcode_dropcap'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/dropcap.js';
		$plgs['shortcode_course_list'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/course-list-table.js';		
		$plgs['shortcode_post_scroller'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/post-scroller.js';
		$plgs['shortcode_member'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/member.js';
		$plgs['shortcode_heading'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/heading.js';
		$plgs['shortcode_countdown'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/shortcode_countdown.js';
		$plgs['shortcode_video_banner'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/video-banner.js';
		$plgs['cactus_compare_table'] = U_SHORTCODE_PLUGIN_URL . 'shortcodes/js/compare-table.js';
		return $plgs;
	}
}

$ctshortcode = new CactusThemeShortcodes();
include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); //for check plugin status
// Register element with visual composer and do shortcode

//include('shortcodes/alert.php');
//include('shortcodes/checklist.php');
//include('shortcodes/member.php');
include('shortcodes/textbox.php');
//include('shortcodes/tooltip.php');
//include('shortcodes/headline.php');
include('shortcodes/button.php');
//include('shortcodes/carousel.php');
include('shortcodes/dropcap.php');
//include('shortcodes/padding.php');
include('shortcodes/heading.php');
//include('shortcodes/smart-content.php');
include('shortcodes/testimonial.php');
//include('shortcodes/pricingtables.php');
include('shortcodes/blog.php');
include('shortcodes/countdown_clock.php');
include('shortcodes/post-carousel.php');
include('shortcodes/post-grid.php');
include('shortcodes/post-scroller.php');
include('shortcodes/video-link.php');
include('shortcodes/compare-table.php');
include('shortcodes/price.php');

//add animation param
function un_add_param() {
	if(class_exists('WPBMap')){
		//get default textblock params
		$shortcode_vc_column_text_tmp = WPBMap::getShortCode('vc_column_text');
		//get animation params
		$attributes = array();
		foreach($shortcode_vc_column_text_tmp['params'] as $param){
			if($param['param_name']=='css_animation'){
				$attributes = $param;
				break;
			}
		}
		if(!empty($attributes)){
			//add animation param
			vc_add_param('textbox', $attributes);
			vc_add_param('ct_button', $attributes);
			vc_add_param('u_heading', $attributes);
			vc_add_param('u_testimonial', $attributes);
			vc_add_param('u_blog', $attributes);
			vc_add_param('u_countdown', $attributes);
			vc_add_param('u_post_carousel', $attributes);
			vc_add_param('u_post_grid', $attributes);
			vc_add_param('u_post_scroller', $attributes);
			vc_add_param('u_video_link', $attributes);
		}
		//delay param
		$delay = array(
			'type' => 'textfield',
			'heading' => __("Animation Delay",'cactusthemes'),
			'param_name' => 'animation_delay',
			'description' => __("Enter Animation Delay in second (ex: 1.5)",'cactusthemes')
		);
		vc_add_param('textbox', $delay);
		vc_add_param('ct_button', $delay);
		vc_add_param('u_heading', $delay);
		vc_add_param('u_testimonial', $delay);
		vc_add_param('u_blog', $delay);
		vc_add_param('u_countdown', $delay);
		vc_add_param('u_post_carousel', $delay);
		vc_add_param('u_post_grid', $delay);
		vc_add_param('u_post_scroller', $delay);
		vc_add_param('u_video_link', $delay);
	}
}
add_action('init', 'un_add_param');

//load animation js
function un_animation_scripts_styles() {
	global $wp_styles;
	wp_enqueue_script( 'waypoints' );
}
add_action( 'wp_enqueue_scripts', 'un_animation_scripts_styles' );

//function
if(!function_exists('hex2rgb')){
	function hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);
	
	   if(strlen($hex) == 3) {
		  $r = hexdec(substr($hex,0,1).substr($hex,0,1));
		  $g = hexdec(substr($hex,1,1).substr($hex,1,1));
		  $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
		  $r = hexdec(substr($hex,0,2));
		  $g = hexdec(substr($hex,2,2));
		  $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = array($r, $g, $b);
	   //return implode(",", $rgb); // returns the rgb values separated by commas
	   return $rgb; // returns an array with the rgb values
	}
}
function u_shortcode_query($post_type='post',$cat='',$tag='',$ids='',$count='',$order='',$orderby='',$meta_key='',$custom_args=''){
	$args = array();
	if($custom_args!=''){ //custom array
		$args = $custom_args;
	}elseif($ids!=''){ //specify IDs
		$ids = explode(",", $ids);
		$args = array(
			'post_type' => $post_type,
			'posts_per_page' => $count,
			'order' => $order,
			'orderby' => $orderby,
			'meta_key' => $meta_key,
			'post__in' => $ids,
			'ignore_sticky_posts' => 1,
		);
	}elseif($ids==''){
		$args = array(
			'post_type' => $post_type,
			'posts_per_page' => $count,
			'order' => $order,
			'orderby' => $orderby,
			//'meta_key' => $meta_key,
			'ignore_sticky_posts' => 1,
		);
		if($post_type=='u_course'){
			if(!is_array($cat) && $cat!='') {
				$cats = explode(",",$cat);
				if(is_numeric($cats[0])){
					$args['tax_query'] = array(
						array(
							'taxonomy' => 'u_course_cat',
							'field'    => 'id',
							'terms'    => $cats,
							'operator' => 'IN',
						)
					);
				}else{
					$args['tax_query'] = array(
						array(
							'taxonomy' => 'u_course_cat',
							'field'    => 'slug',
							'terms'    => $cats,
							'operator' => 'IN',
						)
					);
				}
			}elseif(count($cat) > 0 && $cat!=''){
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'u_course_cat',
						'field'    => 'id',
						'terms'    => $cat,
						'operator' => 'IN',
					)
				);
			}
		}elseif($post_type=='u_event'){
			if(!is_array($cat) && $cat!='') {
				$cats = explode(",",$cat);
				if(is_numeric($cats[0])){
					$args['tax_query'] = array(
						array(
							'taxonomy' => 'u_event_cat',
							'field'    => 'id',
							'terms'    => $cats,
							'operator' => 'IN',
						)
					);
				}else{
					$args['tax_query'] = array(
						array(
							'taxonomy' => 'u_event_cat',
							'field'    => 'slug',
							'terms'    => $cats,
							'operator' => 'IN',
						)
					);
				}
			}elseif(count($cat) > 0 && $cat!=''){
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'u_event_cat',
						'field'    => 'id',
						'terms'    => $cat,
						'operator' => 'IN',
					)
				);
			}
			//tag
			if($tag) {
				$tags = explode(",",$tag);
				$args['tax_query'][] = array(
					'taxonomy' => 'u_event_tags',
					'field'    => 'slug',
					'terms'    => $tags,
					'operator' => 'IN',
				);
			}
		}else{
			if(!is_array($cat)) {
				$cats = explode(",",$cat);
				if(is_numeric($cats[0])){
					$args['category__in'] = $cats;
				}else{			 
					$args['category_name'] = $cat;
				}
			}elseif(count($cat) > 0){
				$args['category__in'] = $cat;
			}
			$args['tag'] = $tag;
		}
		$time_now =  strtotime("now");
		if($orderby=='upcoming'){
			$args += array('meta_key' => 'u-startdate', 'meta_value' => $time_now, 'meta_compare' => '>');
			$args['orderby'] ='meta_value_num';
			if($order==''){$args['order'] ='ASC';}
			//print_r($args);exit;
		}elseif($orderby=='recent'){
			$args += array('meta_key' => 'u-startdate', 'meta_value' => $time_now, 'meta_compare' => '<');
			$args['orderby'] ='meta_value_num';
			if($order==''){$args['order'] ='DESC';}
		}
		$args += array(
		'meta_key' => $meta_key
		);
	}
	$args['post_status'] = $post_type=='attachment'?'inherit':'publish';
	$shortcode_query = new WP_Query($args);
	return $shortcode_query;
}